import axios from 'axios';
import React, { useState } from 'react'
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { Link, useNavigate } from "react-router-dom";
import Swal from 'sweetalert2';

function Login() {

    let navigate = useNavigate();
    const [Email, setEmail] = useState()
    const [Password, setPassword] = useState()

    const LogigApi = () => {
        axios.post('http://localhost:5000/login', {
            Email,
            Password
        }).then((res) => {

            // console.log(res.data.token);
            if (res.status == 200) {
                localStorage.setItem("token",res.data.token)
                    Swal.fire('You are Login')
                navigate(`/Home`);
            } else if (res.msg == "try again") {
                Swal.fire('Try Again sorry ?')
            }
            else if (res.status == 204) {
                Swal.fire('"Invalid Credentials"')
            }


        }).catch((e) => {
            console.log(e);
        })

    }

    return (
        <Form>
            <Form.Group className="mb-3 " controlId="formBasicEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control type="email" placeholder="Enter email"
                    onChange={(e) => setEmail(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" placeholder="Password"
                    onChange={(e) => setPassword(e.target.value)}
                    required
                />
            </Form.Group>

            <Button onClick={(e) => LogigApi()} variant="primary" type="button" className='d-flex justify-content-center'>
                Submit
            </Button>

            <Link to="/register">Register</Link>
        </Form> 
    )
}

export default Login