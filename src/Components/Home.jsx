import axios from 'axios'
import React from 'react'
import { useState } from 'react'
import { useEffect } from 'react'
import { useNavigate } from 'react-router-dom';

export default function Home() {
    let navigate = useNavigate();


    const [user, setUser] = useState()

    const userData = () => {

        const token = localStorage.getItem("token")

        const headers = {
            Authorization: `Bearer ${token}`
        }

        axios.get("http://localhost:5000/user", {
            headers
        }).then((res) => {
            console.log(res.data);
            setUser(res.data)
        }).catch((e) => {
            console.log(e);
        })
    }
    const Logout = () => {

        const token = localStorage.getItem("token")

        const headers = {
            Authorization: `Bearer ${token}`
        }

        axios.post("http://localhost:5000/logout", {
            headers
        }).then((res) => {
            navigate(`/`);
            const token = localStorage.removeItem("token")


        }).catch((e) => {
            console.log(e);
        })
    }


    useEffect(() => {
        userData()

    }, [])

    if (user == undefined) {
        return null
    }
    return (
        <div style={{ marginTop:"20px",marginLeft:"20px"}} onClick={Logout}>
            <button >Logout</button>
            <div style={{ marginTop: "300px", marginLeft: "600px", }}>
                <h1>
                    {user.Name}
                    <div>{user.Email}</div>
                </h1>

            </div>
        </div>
    )
}
