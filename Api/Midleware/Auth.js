const jwt = require("jsonwebtoken")

const User = require("../Model/User")


const Auth = async (req, res, next) => {

    try {

        const token = req.header("Authorization").replace("Bearer ", "")
        const decode = jwt.verify(token, "soyal")
        const user = await User.findOne({ _id: decode._id, "tokens.token": token })

        if (!user) {
            throw error
        }

        req.token = token
        req.user = user
        next()

    } catch (e) {
        res.send("plz login")
    }

}

module.exports = Auth