const User = require("../Model/User")

const router = require("express").Router()
const bcrypt = require("bcrypt")
const Auth = require("../Midleware/Auth")

router.post("/register", async (req, res) => {
    const { Name, Email, Password } = req.body
    try {

        let user = await User.findOne({ Email: req.body.Email })
        if (user) {
            return res.send("Email have already")
        }

        else if (!Name) {
            return res.send("Enter Name")
        }
        else if (!Email) {
            return res.send("Enter Email")
        }
        else if (!Password) {
            return res.send("Enter Password")
        }

        user = new User({
            Name,
            Email,
            Password
        })



        const token = await user.JsonwebToken()

        const salt = await bcrypt.genSalt(10);
        user.Password = await bcrypt.hash(user.Password, salt);


        await user.save()

        res.send({ msg: "you Register", data: Name })

    } catch (e) {
        res.send(e)
        console.log(e);
    }
})



router.post("/login", async (req, res) => {

    const { Email, Password } = req.body

    try {

        const user = await User.findOne({ Email: Email })

        if (!user) {
            return res.status(201).send({ msg: "try again" })
        }


        const isMatch = await bcrypt.compare(Password, user.Password);

        if (!isMatch) {
            return res.status(204).json({ msg: "Invalid Credentials" });
        }


        const token = await user.JsonwebToken()

        res.status(200).send({ token, user })

    } catch (e) {
        res.send(e)
        console.log(e);
    }
})



router.post('/logout', Auth, async (req, res) => {
    try {
        // req.user.tokens = req.user.tokens.filter((token) => {
        //     return token.token !== req.token
        // })
        req.user.tokens = []
        await req.user.save()

        res.send()
        console.log(("ok"));
    } catch (e) {
        res.status(500).send()
    }
})


router.get("/user",Auth,async(req,res)=>{
    try{
         const data =await User.findById(req.user.id)
         res.send(data)
    }catch(e){
        console.log(e);
    }
})


module.exports = router