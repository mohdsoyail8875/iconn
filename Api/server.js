const express = require("express")
const app = express()
const port = 5000
var bodyParser = require('body-parser')

const cors = require("cors")

const mongoose = require("mongoose")
mongoose.connect("mongodb://localhost:27017/Interview", {

}, (err) => {
    if (!err) {
        console.log("yes");
    } else {
        console.log("no");
    }
})

app.use(bodyParser.urlencoded({ extended: false }))

app.use(bodyParser.json())


app.use(cors())


app.use("/", require("./Routes/User"))


app.listen(port, (req, res) => {
    console.log(`port stared at ${port}`);
})

